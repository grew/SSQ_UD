% =============================================================================================
% Multiword expressions
package projection_arcs {
  rule mwe1 {
    pattern{
      P[cat=P,_UD_mw_span="2"]; DET[cat=D]; P < DET;
      MWE -[MWE]-> P;
    }
    without { MWE -[MWE]-> DET; }
    without { MWE -["1"]-> P; }
    commands { add_edge MWE -["1"]-> P; }
  }

  rule mwe2 {
    pattern{
      P[cat=P,_UD_mw_span="2"]; DET[cat=D]; P < DET;
      MWE -[MWE]-> DET;
    }
    without { MWE -[MWE]-> P; }
    without { MWE -["2"]-> P; }
    commands { add_edge MWE -["2"]-> P; }
  }

  rule ne1 {
    pattern{
      P[cat=P,_UD_mw_span="2"]; DET[cat=D]; P < DET;
      NE -[NE]-> P;
    }
    without { NE -[NE]-> DET; }
    without { NE -["1"]-> P; }
    commands { add_edge NE -["1"]-> P; }
  }

  rule ne2 {
    pattern{
      P[cat=P,_UD_mw_span="2"]; DET[cat=D]; P < DET;
      NE -[NE]-> DET;
    }
    without { NE -[NE]-> P; }
    without { NE -["2"]-> P; }
    commands { add_edge NE -["2"]-> P; }
  }
}

package contraction{

  rule contract (lex from "../lexicons/word_class/contraction_comp.lp"){
    pattern{
      P[cat=P,form=lex.prep,_UD_mw_span="2"]; DET[cat=D,form=lex.det]; P < DET
    }
    commands{
      shift DET =[MWE|NE]=> P;
      del_node DET;
      P.cat = "P+D";  P.pos = "P+D";P.form=lex.form;P.s=def;
      del_feat P._UD_mw_fusion; del_feat P._UD_mw_span;
    }
  }

  rule p_pro {
    pattern{
      P[form=lex.prep];
      PRO[form=lex.pro];
      P < PRO;
    }
    commands{
      del_node PRO;
      P.cat = "P+PRO";  P.pos = "P+PRO"; P.form=lex.amal;
      del_feat P._UD_mw_fusion; del_feat P._UD_mw_span;
    }
  }
#BEGIN lex
prep	pro	amal
%----------
à	lequel	auquel
à	lesquels	auxquels
à	lesquelles	auxquelles
À	lequel	Auquel

de	lequel	duquel
de	lequels	desquels
de	lequelles	desquelles
#END

}

% =============================================================================================
% Correction of dependencies
package dep_sequoia_correction {

  rule prep_dep{
    pattern{
      P[cat=P]; P -[obj.p]-> OBJP;
      e: OBJP -[mod]-> MOD; MOD << P;
    }
    commands{
      del_edge e;
      add_edge P -[mod]-> MOD
    }
  }

  % Fix annotation "beaucoup de N"
  rule quant-adv_noun {
    pattern {
      ADV[cat=ADV]; DE[cat=P,form="de"]; N[cat=N|PRO];
      det: N -[det]-> ADV; dep: ADV -[dep]-> DE
    }
    commands {
      del_edge det; del_edge dep;
      add_edge N -[mod]-> ADV; add_edge N -[det]-> DE
    }
  }
}

% =============================================================================================
% Deletion of features
package del_f{

  rule del_diat{
    pattern{V[diat]}
    commands{del_feat V.diat}}

  rule del_init_upos{
    pattern{C[cat,init_upos]}
    commands{C.cat=C.init_upos; del_feat C.init_upos}}

}
