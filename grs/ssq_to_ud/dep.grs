% =============================================================================================
% Raising the mood of an auxiliary to the main verb.
package verb_mood {

% Tense auxiliaries.
  rule aux_tense {
    pattern { AUX[upos=AUX,VerbForm];V -[aux.tps]-> AUX; V[VerbForm] }
    without { V[initVerbForm] }
    commands { V.initVerbForm = V.VerbForm;V.VerbForm=AUX.VerbForm }
  }

% Passive auxiliaries.
  rule aux_pass {
    pattern { AUX[upos=AUX,VerbForm]; V -[aux.pass]-> AUX; V[VerbForm] }
    without { AUX[VerbForm=Part,Tense=Past] }
    without { V[initVerbForm] }
    commands { V.initVerbForm = V.VerbForm;V.VerbForm=AUX.VerbForm;V.Voice=Pass }
  }

% Causative auxiliaries.
  rule aux_caus {
    pattern { AUX[upos=AUX,VerbForm];V -[aux.caus]-> AUX; V[VerbForm] }
    without { AUX[VerbForm=Part,Tense=Past] }
    without { V[initVerbForm] }
    commands { V.initVerbForm = V.VerbForm;V.VerbForm=AUX.VerbForm }
  }

}

% =============================================================================================
% Changing the labels of the verb core dependencies.
package verb_core {

% Tense auxiliaries.
  rule aux {
    pattern {auxtps_rel: V -[aux.tps]-> AUX }
    commands { del_edge auxtps_rel; add_edge V -[aux:tense]-> AUX }
  }

% Passive auxiliaries.
  rule auxpass {
    pattern {auxpass_rel: V -[aux.pass]-> AUX }
    commands { del_edge auxpass_rel; add_edge V -[aux:pass]-> AUX; V.Voice=Pass }
  }

% Causative auxiliaries.
  rule auxcaus {
    pattern { auxcaus_rel: V -[aux.caus]-> AUX }
    commands { del_edge auxcaus_rel; add_edge V -[aux:caus]-> AUX }
  }

% Nominal dependents of verbs that are not semantic arguments (affixes).
  rule expl_comp_impers {
    pattern { AFF[upos=PRON|ADV, impers=y]; aff_rel: V -[aff]-> AFF} % ADV for "[annodis.er_00188] Il n'empêche que…"
    commands { del_edge aff_rel; add_edge V -[expl:comp]-> AFF; del_feat AFF.impers }
  }

  rule expl_comp {
    pattern { AFF[upos=PRON|ADV, !impers]; aff_rel: V -[aff]-> AFF} % ADV for "[annodis.er_00188] Il n'empêche que…"
    commands { del_edge aff_rel; add_edge V -[expl:comp]-> AFF }
  }

  rule expl_pass_impers {
    pattern { AFF[upos=PRON|ADV, impers=y]; aff_rel: V -[aff.demsuj]-> AFF} % ADV for "[annodis.er_00188] Il n'empêche que…"
    commands { del_edge aff_rel; add_edge V -[expl:pass]-> AFF; del_feat AFF.impers }
  }

  rule expl_pass {
    pattern { AFF[upos=PRON|ADV, !impers]; aff_rel: V -[aff.demsuj]-> AFF} % ADV for "[annodis.er_00188] Il n'empêche que…"
    commands { del_edge aff_rel; add_edge V -[expl:pass]-> AFF }
  }
}


rule no_child_for_cc {
  pattern { H -[conj]-> DEP;  DEP -[cc]-> CC; e: CC -[mod|ponct]-> M; }
  commands { add_edge f: DEP -> M; f.label = e.label; del_edge e }
}


% =============================================================================================
% Change the head of expressions with a preposition or a conjunction of subordination.
package head_chg {

% Precondition : a subordinating conjunction governs a term with an OBJ.CPL dependency.
% Action : the term becomes the governor of the conjunction with a MARK dependency and all dependencies incident to the conjunction are shifted to the term.
  rule conj_objcpl {
    pattern { objcpl_rel: CS -[obj.cpl]-> DEP }
    without { * -[obj.cpl]-> CS }
    without { CS -[obj.cpl]-> X; X << DEP }
    commands {
      del_edge objcpl_rel;
      shift CS =[1<>dep_cpd|parseme|frsemcor]=> DEP;
      add_edge DEP -[mark]-> CS
    }
  }

% Precondition : a preposition governs a nominal term with an OBJ.P dependency.
% Action : the term becomes the governor of the preposition with a CASE dependency  and all dependencies incident to the preposition are shifted to the term.
  rule prep_objp_noun {
    pattern {
      PREPOBJ[upos=ADV|DET|NOUN|NUM|PRON|PROPN|X];
      objp_rel: PREP -[obj.p]-> PREPOBJ
    }
    without { PREP2[upos=ADP]; PREP -[conj]-> PREP2 }
    commands {
      del_edge objp_rel;
      shift PREP =[1<>dep_cpd|parseme|frsemcor]=> PREPOBJ;
      add_edge PREPOBJ -[case]-> PREP
    }
  }

% Precondition : a preposition governs a nominal term with an OBJ.P dependency; it also governs a preposition that governs nothing.
% Action : the term becomes the governor of the preposition with a CASE dependency  and all dependencies incident to the preposition are shifted to the term, except the dependency to the second preposition.
  rule prep-conj_objp_noun {
    pattern {
      PREPOBJ[upos=ADV|DET|NOUN|NUM|PRON|PROPN|X];
      objp_rel: PREP -[obj.p]-> PREPOBJ;
      PREP2[upos=ADP];  PREP -[conj]-> PREP2
    }
    without { PREP2 -[^ cc]-> * }
    commands {
      del_edge objp_rel;
      shift PREP =[1<>conj|dep_cpd|parseme|frsemcor]=> PREPOBJ;
      add_edge PREPOBJ -[case]-> PREP
    }
  }
% Precondition : a preposition governs an adjective used as a nominal term with an OBJ.P dependency.
% Action : the adjective becomes the governor of the preposition with a CASE dependency  and all dependencies incident to the preposition are shifted to the adjective.
  rule prep_objp_adj-det {
    pattern { PREPOBJ -[det]-> *; objp_rel: PREP -[obj.p]-> PREPOBJ }
    commands {
      del_edge objp_rel;
      shift PREP =[1<>dep_cpd|parseme|frsemcor]=> PREPOBJ;
      add_edge PREPOBJ -[case]-> PREP
    }
  }

% Precondition : a preposition governs a predicative term with an OBJ.P dependency.
% Action : the term becomes the governor of the preposition with a CASE dependency and all dependencies incident to the preposition are shifted to the term .
  rule prep_objp_pred {
    pattern {
      PREPOBJ[upos=ADJ|VERB];
      objp_rel: PREP -[obj.p]-> PREPOBJ
    }
    without { PREPOBJ -[det]-> * }
    commands {
      del_edge objp_rel;
      shift PREP =[1<>det|dep_cpd|parseme|frsemcor]=> PREPOBJ;
      add_edge PREPOBJ -[mark]-> PREP
    }
  }

% Precondition : there is a MOD dependency from a noun to an adverb from the lexicon below and a DET dependency from the noun to a prepostion "de".
% Action : the adverb with the preposition becomes a compound determiner of the noun.
  rule adv_de_noun {
    pattern {
      N [upos=NOUN|PRON]; det_rel:N -[det]-> DE; DE[upos=ADP,lemma="de"];
      mod_rel: N -[mod]-> ADV; ADV [upos=ADV, lemma=lex.lemma]
    }
    commands {
      del_edge det_rel; del_edge mod_rel;
      add_edge N -[case]-> DE; add_edge ADV -[obl:arg]-> N;
      shift_in N ==> ADV;
      shift_out N =[orphan|cc]=> ADV;
%      add_edge ADV -[fixed]-> DE; add_edge N -[det]-> ADV;
    }
  }
#BEGIN lex
lemma
%----
peu
plus
moins
autant
beaucoup
davantage
suffisamment
trop
combien
pas
#END

  % complement to the previous rule: only modifier that are before the adverb should change their head
  rule shift_left_mod_after_adv_de_noun {
    pattern {
      ADV [upos=ADV, lemma=lex.lemma];
      ADV -[obl:arg]-> N; e: N -[mod]-> M; M << ADV
    }
    commands {
      del_edge e;
      add_edge ADV -[mod]-> M;
    }
  }
#BEGIN lex
lemma
%----
peu
plus
moins
autant
beaucoup
davantage
suffisamment
trop
combien
pas
#END




% "moins de 1 pour 1000" where "de" in "dep"  in Sequoia
  rule adv_de_dep_noun {
    pattern {
      N [upos=NOUN|PRON]; dep_rel:N -[dep]-> DE; DE[upos=ADP,lemma="de"];
      mod_rel: N -[mod]-> ADV; ADV [upos=ADV, lemma=lex.lemma];
      ADV < DE;
    }
    commands {
      del_edge dep_rel; del_edge mod_rel;
      add_edge ADV -[fixed]-> DE; add_edge N -[advmod]-> ADV
    }
  }
#BEGIN lex
lemma
%----
peu
plus
moins
autant
beaucoup
davantage
suffisamment
trop
combien
pas
#END

% Precondition : there is a MOD dependency from a noun to an adverb "moins" or "plus", a DET dependency from the noun and a DEP dependency from the noun to a prepostion "de".
% Action : the adverb with the preposition becomes a compound modifier of the noun.
  rule adv_de_det_noun {
    pattern {
      N [upos=NOUN|PRON]; N -[det]-> DET;
      DE [upos=ADP,lemma="de"]; dep_rel: N -[dep]-> DE;
      ADV [upos=ADV, lemma="moins"|"plus"]; mod_rel: N -[mod]-> ADV;
      ADV < DE;
    }
    commands {
      del_edge dep_rel; del_edge mod_rel;
      add_edge ADV -[fixed]-> DE; add_edge DET -[advmod]-> ADV
    }
  }

% Precondition : there is DEP dependency from a foreign word to another foreign word before it.
% Action: replace the dependency with FLAT:FOREIGN dependency in the opposite direction.
  rule x_dep_x {
    pattern { X1[upos=X]; X2[upos=X]; dep_rel: X1 -[dep]-> X2; X2 << X1 }
    without { X[upos=X]; X1 -[dep]-> X; X2 << X; X << X1 }
    commands { del_edge dep_rel; shift X1 =[1<>parseme|frsemcor]=> X2; add_edge X2 -[flat:foreign]-> X1 }
  }

% Precondition : there is a DEP_CPD dependency to a noun with a determiner.
% Action : the determiner is linked to the source of the DEP_CPD with a new DEP_CPD relation

% special rule for construction "jusqu'à + NUM + NP"
  rule jusqu_a {
    pattern {
      J [lemma=jusque]; A [lemma="à"]; J -[obj.p]-> A;
      N [upos=NOUN]; D[upos=NUM|DET]; N -[det]-> D;
      N -[mod]-> J;
    }
    commands {
      del_edge N -[mod]-> J; add_edge N -[case]-> J;
      del_edge J -[obj.p]-> A; add_edge N -[case]-> A;
    }
  }
}

% =============================================================================================
package first_name_last_name {
  rule m_mme_dr {
    pattern {
      N [upos=NOUN, lemma="monsieur"|"madame"|"Docteur"];
      N1 [upos=PROPN]; N2 [upos=PROPN]; N1 < N2;
      e1:N -[mod]-> N1; e2:N -[mod]-> N2;
    }
    commands {
      del_edge e1; del_edge e2;
      add_edge N -[flat:name]-> N1;
      add_edge N -[flat:name]-> N2;
    }
  }

  % 2 next rules: tranform structures with 1 NOUN, followed by n≥2 PROPN
  rule other_nouns_final {
    pattern {
      N [upos=NOUN, lemma<>"monsieur"|"madame"|"Docteur"];
      N1 [upos=PROPN]; N2 [upos=PROPN]; N1 < N2;
      e1:N -[mod]-> N1; e2:N -[mod]-> N2; }
    without {N0 [upos=PROPN]; N0 < N1; N -[mod]-> N0}
    without {N3 [upos=PROPN]; N2 < N3; N -[mod]-> N3}
    commands {
      del_edge e1; del_edge e2;
      add_edge N -[appos]-> N1;
      add_edge N1 -[flat:name]-> N2;
    }
  }

  rule other_nouns_iter {
    pattern {
      N [upos=NOUN, lemma<>"monsieur"|"madame"|"Docteur"];
      N1 [upos=PROPN]; N2 [upos=PROPN]; N3 [upos=PROPN]; N1 < N2; N2 < N3;
      N -[mod]-> N1; N -[mod]-> N2; e:N -[mod]-> N3}
    without {N4 [upos=PROPN]; N3 < N4; N -[mod]-> N4}
    commands {
      del_edge e;
      add_edge N1 -[flat:name]-> N3;
      shift_out N2 =[flat:name]=> N1;
    }
  }




}

% =============================================================================================
% Changing dependency labels from the SSQ format to the UD format for the dependencies governed by predicates.
package label_preddep_sq_ud {

  rule obj_agent {
    pattern { e: V -[obj]-> O; O [agent=y] }
    commands {
      del_edge e;
      del_feat O.agent;
      add_edge V -[obj:agent]-> O;
    }
  }

% An adjective modifier of an adjective.
  rule adj_mod_adj {
    pattern {
      ADJ1[upos=ADJ]; ADJ2[upos=ADJ];
      mod_rel: ADJ1 -[mod]-> ADJ2
    }
    without { ADJ2 -[case|mark]-> * }
    without { ADJ2 -[det]-> * }
    commands { del_edge mod_rel; add_edge ADJ1 -[amod]-> ADJ2 }
  }

% A predicate introduced with a preposition modifier of an adjective.
  rule adj_mod_pred-mark {
    pattern {
      ADJ[upos=ADJ]; PRED[upos=ADJ|VERB]; PRED -[mark]-> *;
      mod_rel: ADJ -[mod]-> PRED
    }
    commands { del_edge mod_rel; add_edge ADJ -[advcl]-> PRED }
  }

% Cardinals used as modifiers or determiners.
  rule cat_det-mod_num {
    pattern { NUM [upos=NUM]; num_rel: N -[det|mod]-> NUM }
    without { NUM -[case]-> * }
    commands { del_edge num_rel; add_edge N -[nummod]-> NUM }
  }

% Finite or infinitive verbs heads of clauses juxtaposed.
  rule cat_mod_verb {
    pattern { V[upos=VERB, VerbForm=Fin|Inf]; mod_rel: C -[mod]-> V }
    without { V -[mark]-> * }
    commands { del_edge mod_rel; add_edge C -[parataxis]-> V }
  }

% An ARG dependency from a noun introduced with a preposition "de" to a noun introduced with a preposition "à" is replaced with a NMOD dependency.
  rule de_noun_a_noun {
    pattern {
      N1[upos=ADJ|NOUN|NUM|PRON|PROPN];
      DE[upos=ADP,lemma="de"]; N1 -[case]-> DE;
      N2[upos=ADJ|NOUN|NUM|PRON|PROPN]; arg_rel: N1 -[arg]-> N2;
      A[upos=ADP,lemma="à"]; N2 -[case]-> A
    }
    commands { del_edge arg_rel; add_edge N1 -[nmod]-> N2 }
  }

% Nominal subjects of verbs that are not in the passive voice.
  rule noun_suj_verb {
    pattern { N[upos=ADV|ADJ|DET|NOUN|PRON|PROPN|NUM|X, !impers]; suj_rel: V -[suj]-> N }
    without { V[Voice=Pass] }
    without { V -[aux:caus]-> * }
    commands { del_edge suj_rel; add_edge V -[nsubj]-> N }
  }

% Impersonnal subject
  rule impers_suj {
    pattern { N[upos=ADV|ADJ|DET|NOUN|PRON|PROPN|NUM|X, impers=y]; suj_rel: V -[suj]-> N }
    without { V[Voice=Pass] }
    without { V -[aux:caus]-> * }
    commands { del_edge suj_rel; add_edge V -[expl:subj]-> N; del_feat N.impers }
  }

% Nominal subjects of verbs in the passive voice.
  rule noun_suj_verb-pass {
    pattern {
      V[upos=VERB,Voice=Pass]; N[upos=ADV|ADJ|NOUN|PRON|PROPN|NUM|X];
      suj_rel: V -[suj]-> N
    }
    without { V -[aux:caus]-> * }
    commands { del_edge suj_rel; add_edge V -[nsubj:pass]-> N }
  }

% Nominal subjects of verbs in the causative construction
  rule noun_suj_verb-caus {
    pattern {
      V[upos=VERB]; N[upos=ADJ|NOUN|PRON|PROPN|NUM];
      suj_rel: V -[suj]-> N;
      V -[aux:caus]-> *;
    }
    commands { del_edge suj_rel; add_edge V -[nsubj:caus]-> N }
  }

% Adverbs governed by a predicate.
% Comments : when an adverb is a modifier of a predicate, the dependency is ADVMOD.
  rule pred_arg_adv {
    pattern {
      PRED[upos=ADJ|ADV|VERB]; ADV[upos=ADV];
      arg_rel: PRED -[a_obj|de_obj|p_obj.o]-> ADV
    }
    commands { del_edge arg_rel; add_edge PRED -[obl:arg]-> ADV }
  }

% Infinitive governed by a predicate
% Comments: strictly the subject of the infinitives must be controlled by the governor verb, which requires a lexicon.
  rule pred_arg_inf {
    pattern {
      PRED[upos=ADJ|ADV|VERB]; INF[upos=VERB,VerbForm=Inf,!deep_subj];
      arg_rel: PRED -[a_obj|de_obj|obj|p_obj.o]-> INF
    }
    without { INF -[det]->* }
    commands { del_edge arg_rel; add_edge PRED -[xcomp]-> INF }
  }

  rule pred_arg_inf_deep_subj {
    pattern {
      PRED[upos=ADJ|ADV|VERB]; INF[upos=VERB,VerbForm=Inf,deep_subj=y];
      arg_rel: PRED -[a_obj|de_obj|obj|p_obj.o]-> INF
    }
    commands { del_edge arg_rel; add_edge PRED -[csubj]-> INF }
  }



% Verb governed by a predicate or an adverb without control.
  rule pred_arg_verb {
    pattern {
      PRED[upos=ADJ|ADV|VERB]; V[upos=VERB, VerbForm=Fin|Part];
      arg_rel: PRED -[a_obj|dep|de_obj|obj|p_obj.o]-> V
    }
    without { PRED -[det|nummod]-> * }
    commands { del_edge arg_rel; add_edge PRED -[ccomp]-> V }
  }

% Word with a determiner that is an indirect complement of a predicate.
  rule pred_compl_cat-det_arg {
    pattern {
      PRED[upos=ADJ|ADV|VERB];
      ADJ[upos=ADJ]; ADJ -[det]-> *;
      mod_rel: PRED -[a_obj|de_obj|p_obj.o]-> ADJ
    }
    commands { del_edge mod_rel; add_edge PRED -[obl:arg]-> ADJ }
  }

% Word with a determiner that is an indirect complement of a predicate.
  rule pred_compl_cat-det_mod {
    pattern {
      PRED[upos=ADJ|ADV|VERB];
      ADJ[upos=ADJ]; ADJ -[det]-> *;
      mod_rel: PRED -[dep|mod]-> ADJ
    }
    commands { del_edge mod_rel; add_edge PRED -[obl:mod]-> ADJ }
  }

% Nominal indirect complement of a predicate.
% Europar.550_00292 …j'insiste sur "générale"…
% frwiki_50.1000_00012 …choisis parmi les plus jeunes…
  rule pred_compl_adj {
    pattern {
      PRED[upos=ADJ|ADV|VERB]; N[upos=ADJ];
      mod_rel: PRED -[p_obj.o]-> N
    }
    without { PRED -[det]-> * }
    commands { del_edge mod_rel; add_edge PRED -[obl:arg]-> N }
  }

% Nominal indirect complement of a predicate: agent
  rule pred_compl_noun_agent {
    pattern {
      PRED[upos=ADJ|ADV|VERB]; N[upos=NOUN|NUM|PROPN|X];
      mod_rel: PRED -[p_obj.agt]-> N
    }
    without { PRED -[det]-> * }
    commands { del_edge mod_rel; add_edge PRED -[obl:agent]-> N }
  }

% Nominal indirect complement of a predicate: argument
  rule pred_compl_noun_arg {
    pattern {
      PRED[upos=ADJ|ADV|VERB]; N[upos=NOUN|NUM|PROPN|X, !agent];
      mod_rel: PRED -[a_obj|de_obj|p_obj.o]-> N
    }
    commands { del_edge mod_rel; add_edge PRED -[obl:arg]-> N }
  }

  rule pred_compl_noun_arg_agent {
    pattern {
      PRED[upos=ADJ|ADV|VERB]; N[upos=NOUN|NUM|PROPN|X, agent=y];
      mod_rel: PRED -[a_obj|de_obj|p_obj.o]-> N
    }
    commands { del_edge mod_rel; add_edge PRED -[obl:agent]-> N; del_feat N.agent; }
  }

% Nominal indirect complement of a predicate: modifier
  rule pred_compl_noun_mod {
    pattern {
      PRED[upos=ADJ|ADV|VERB]; N[upos=NOUN|NUM|PROPN|X];
      mod_rel: PRED -[mod]-> N
    }
    without { PRED -[det]-> * }
    commands { del_edge mod_rel; add_edge PRED -[obl:mod]-> N }
  }

% Pronoun indirect complement of a predicate without preposition.
  rule pred_compl_pro {
    pattern {
      PRED[upos=ADJ|VERB]; PRO[upos=PRON,lemma <> chacun, !agent];
      compl_rel: PRED -[a_obj|de_obj|p_obj.o|mod]-> PRO
    }
    without { PRO -[case]-> * }
    without { PRED -[det]-> * }
    commands { del_edge compl_rel; add_edge PRED -[iobj]-> PRO }
  }

% Pronoun indirect complement of a predicate without preposition.
  rule pred_compl_pro_agent {
    pattern {
      PRED[upos=ADJ|VERB]; PRO[upos=PRON,lemma <> chacun, agent=y];
      compl_rel: PRED -[a_obj|de_obj|p_obj.o|mod]-> PRO
    }
    without { PRO -[case]-> * }
    without { PRED -[det]-> * }
    commands { del_edge compl_rel; add_edge PRED -[iobj:agent]-> PRO; del_feat PRO.agent; }
  }

% Pronoun indirect complement of a predicate or an adverb introduced with a preposition.
  rule pred_compl_pro-prep_arg {
    pattern {
      PRED[upos=ADJ|ADV|VERB]; PRO[upos=PRON]; PRO -[case]-> *;
      mod_rel: PRED -[a_obj|de_obj|p_obj.o]-> PRO
    }
    without { PRED -[det]-> * }
    commands { del_edge mod_rel; add_edge PRED -[obl:arg]-> PRO }
  }

% Pronoun indirect complement of a predicate or an adverb introduced with a preposition.
  rule pred_compl_pro-prep_agent {
    pattern {
      PRED[upos=ADJ|ADV|VERB]; PRO[upos=PRON]; PRO -[case]-> *;
      mod_rel: PRED -[p_obj.agt]-> PRO
    }
    without { PRED -[det]-> * }
    commands { del_edge mod_rel; add_edge PRED -[obl:agent]-> PRO }
  }

% Modifier pronoun or adverb introduced with a preposition.
  rule pred_compl_pro-prep_mod {
    pattern {
      PRED[upos=ADJ|ADV|VERB]; PRO[upos=PRON|ADV]; PRO -[case]-> *;
      mod_rel: PRED -[mod]-> PRO
    }
    without { PRED -[det]-> * }
    commands { del_edge mod_rel; add_edge PRED -[obl:mod]-> PRO }
  }

% Double subjects.
  rule suj_verb_suj_il {
    pattern {
      V[upos=VERB]; V -[csubj|csubj:pass|nsubj|nsubj:pass]-> SUBJ; SUBJ << V;
      CL[upos=PRON,lemma="il"]; SUBJ << CL;
      subj_rel: V -[csubj|csubj:pass|nsubj|nsubj:pass]-> CL
    }
    commands { del_edge subj_rel;  add_edge V -[expl:subj]-> CL }
  }

% Predicative complement of a verb, which is not a finite verb.
  rule verb_ato-ats_cat {
    pattern { V[upos=VERB|ADJ];at_rel: V -[ato|ats]-> AT }
    without {AT[upos=VERB,VerbForm=Fin] }
    without { V[upos=VERB,lemma="être"]; AT[upos <>VERB] }
    commands { del_edge at_rel; add_edge V -[xcomp]-> AT }
  }

% Subject predicative complement of a verb, which is a finite verb.
  rule verb_ats_verb-fin {
    pattern { V[upos=VERB];ATS[upos=VERB,VerbForm=Fin]; ats_rel: V -[ats]-> ATS }
    commands { del_edge ats_rel; add_edge V -[ccomp]-> ATS }
  }

% Adjective that is not introduced with a preposition modifier of a verb.
  rule verb_mod_adj {
    pattern {
      V[upos=VERB]; ADJ[upos=ADJ];
      mod_rel: V -[mod]-> ADJ
    }
    without {ADJ -[case]-> * }
    without { V -[det|nummod]-> * }
    commands { del_edge mod_rel; add_edge V -[advmod]-> ADJ }
  }

% Adjective that is introduced with a preposition modifier of a verb.
  rule verb_mod_adj-prep {
    pattern {
      V[upos=VERB]; ADJ[upos=ADJ];
      ADJ -[case]-> *;
      mod_rel: V -[mod]-> ADJ
    }
    without { V -[det|nummod]-> * }
    commands { del_edge mod_rel; add_edge V -[advcl]-> ADJ }
  }

% Verb modifier of a verb.
  rule verb_mod_verb {
    pattern {
      V[upos=VERB]; VCOMPL[upos=VERB];
      mod_rel: V -[mod]-> VCOMPL
    }
    without { VCOMPL -[det|nummod]-> * }
    without { V -[det|nummod]-> * }
    commands { del_edge mod_rel; add_edge V -[advcl]-> VCOMPL }
  }

% Verb modifier of a verb.
  rule verb_mod_verb_cleft {
    pattern {
      V[upos=VERB]; VCOMPL[upos=VERB];
      mod_rel: V -[mod.cleft]-> VCOMPL;
    }
    without { VCOMPL -[det|nummod]-> * }
    without { V -[det|nummod]-> * }
    commands {
      del_edge mod_rel; add_edge V -[advcl:cleft]-> VCOMPL;
    }
  }

% Clausal subjects of verbs in the active voice.
  rule verb_suj_verb {
    pattern { V1[upos=VERB]; V2[upos=VERB]; suj_rel: V2 -[suj]-> V1}
    without { V2[Voice=Pass]}
    commands { del_edge suj_rel; add_edge V2 -[csubj]-> V1 }
  }

% Clausal subjects of verbs in the passive voice.
  rule verb_suj_verb-pass {
    pattern { V1[upos=VERB]; V2[upos=VERB,Voice=Pass]; suj_rel: V2 -[suj]-> V1}
    commands { del_edge suj_rel; add_edge V2 -[csubj:pass]-> V1 }
  }

}

% =============================================================================================
% Changing dependency labels from the Sequoia format to the Universal Dependency format for noun dependencies.
package label_noundep_sq_ud {

% Nouns modifiers of heads of noun phrases, which are not nouns.
  rule upos-det_dep_noun {
    pattern {
      C -[det]-> *; N[upos=NOUN|PRON|PROPN];
      dep_rel: C -[dep]-> ADJ
    }
    without { C[upos=NOUN|PRON|PROPN] }
    commands { del_edge dep_rel; add_edge C -[nmod]-> ADJ }
  }

% Adjectival modifiers of heads of noun phrases, which are not nouns.
  rule upos-det_mod_adj {
    pattern {
      C -[det]-> *; ADJ[upos=ADJ];
      mod_rel: C -[mod]-> ADJ
    }
    without { C[upos=NOUN|PRON|PROPN] }
    without { ADJ -[det]-> * }
    commands { del_edge mod_rel; add_edge C -[amod]-> ADJ }
  }

% Verb modifiers of heads of noun phrases, which are not nouns.
  rule upos-det_dep-mod_verb {
    pattern {
      C -[det]->*;
      V[upos=VERB];
      mod_rel: C -[dep|mod]-> V
    }
    without { C[upos=NOUN|PRON|PROPN] }
    without { V -[det]-> * }
    commands { del_edge mod_rel; add_edge C -[acl]-> V }
  }

% Adjectives complement of a noun.
  rule noun_dep_adj {
    pattern {
      N[upos=NOUN|PRON|PROPN];  ADJ[upos=ADJ];
      dep_rel: N -[dep]-> ADJ
    }
    without { ADJ -[det]-> * }
    commands { del_edge dep_rel; add_edge N -[acl]-> ADJ }
  }

% Heads of noun phrases complement of a noun.
  rule noun_dep_cat-det {
    pattern {
      N[upos=NOUN|PRON|PROPN];  C -[det]-> DET;
      dep_rel: N -[dep]-> C
    }
    without { C[upos=NOUN|PRON|PROPN] }
    without {
      C[upos=ADJ];ADV[upos=ADV,lemma="mieux"|"moins"|"plus"];
      C -[mod]-> ADV; DET[upos=DET,lemma="le"]
    }
    commands { del_edge dep_rel; add_edge N -[nmod]-> C }
  }

% Nouns complement of a noun.
  rule noun_dep_noun {
    pattern {
      N1[upos=NOUN|PRON|PROPN];  N2[upos=NOUN|PRON|PROPN];
      dep_rel: N1 -[dep]-> N2
    }
    commands { del_edge dep_rel; add_edge N1 -[nmod]-> N2 }
  }

% Verbs modifiers of nouns.
  rule noun_dep-mod_verb {
    pattern {
      N[upos=NOUN|PRON|PROPN|ADJ]; V [upos=VERB];
      mod_rel: N -[dep|mod]-> V
    }
    without { V -[det]-> * }
    commands { del_edge mod_rel; add_edge N -[acl]-> V }
  }

% Adjectival modifiers of a noun.
  rule noun_mod_adj {
    pattern {
      N[upos=NOUN|PRON|PROPN]; ADJ[upos=ADJ];
      mod_rel: N -[mod]-> ADJ
    }
    without { ADJ -[det]-> * }
    without { ADJ -[case|mark]-> * }
    commands { del_edge mod_rel; add_edge N -[amod]-> ADJ }
  }

% Adjectival modifiers of a noun introduced with a conjunction.
  rule noun_mod_adj-conj {
    pattern {
      N[upos=NOUN|PRON|PROPN]; ADJ[upos=ADJ]; ADJ -[mark]-> *;
      mod_rel: N -[mod]-> ADJ
    }
    without { ADJ -[det]-> * }
    commands { del_edge mod_rel; add_edge N -[acl]-> ADJ }
  }

% Superlatives modifier of a noun.
  rule noun_mod_adj-super {
    pattern {
      N[upos=NOUN|PRON|PROPN]; ADJ[upos=ADJ];
      mod_rel: N -[mod]-> ADJ;
      ADV[upos=ADV,lemma="mieux"|"moins"|"plus"]; ADJ -[mod]-> ADV;
      DET[upos=DET,lemma="le"];ADJ -[det]-> DET
    }
    without { ADJ -[case]-> * }
    commands { del_edge mod_rel; add_edge N -[amod]-> ADJ }
  }

% Common nouns modifiers of other common nouns.
  rule noun_mod_noun(lex from "lexicons/titles.lp"){
    pattern { N1[upos=NOUN|NUM|PRON|PROPN];N2[upos=NOUN|NUM|PRON|PROPN]; dep_rel: N1 -[dep|mod]-> N2 }
    without { N1[lemma=lex.lemma]; N2[upos=PROPN] }
    commands { del_edge dep_rel; add_edge N1 -[nmod]-> N2 }
  }

% Common nouns modifiers of adj as noun.
  rule adj_mod_noun {
    pattern { N1[upos=ADJ];N2[upos=NOUN|NUM|PRON|PROPN]; N1 -[det]-> D; dep_rel: N1 -[dep|mod]-> N2 }
    commands { del_edge dep_rel; add_edge N1 -[nmod]-> N2 }
  }

 % Title nouns governing proper nouns.
  rule noun-title_mod_propn(lex from "lexicons/titles.lp") {
    pattern {
      N1 [upos=NOUN, lemma = lex.lemma];
      N2 [upos=PROPN]; N1 << N2; mod_rel: N1 -[mod]-> N2
    }
    without { PUNCT[upos=PUNCT]; N1 << PUNCT; PUNCT << N2 }
    commands { del_edge mod_rel; add_edge N1 -[flat:name]-> N2 }
  }

% Nominal appositions of nouns.
  rule noun_modapp_noun {
    pattern { modapp_rel: N -[mod.app]-> APP }
    commands { del_edge modapp_rel; add_edge N -[appos]-> APP }
  }

% Relative clauses.
  rule noun_modrel {
    pattern { modrel_rel: N -[mod.rel]-> V }
    commands { del_edge modrel_rel; add_edge N -[acl:relcl]-> V }
  }

% Proper nouns governing a common noun.
  rule propn_mod_noun {
    pattern {
      N1 [upos=PROPN];
      N2 [upos=NOUN|PRON]; mod_rel: N1 -[mod]-> N2
    }
    without { N2 -[case]-> * }
    commands { del_edge mod_rel; add_edge N1 -[nmod]-> N2 }
  }

% Proper nouns governing a common noun.
  rule noun_mod_x {
    pattern {
      N1 [upos=NOUN];
      N2 [upos=X]; mod_rel: N1 -[mod]-> N2
    }
    commands { del_edge mod_rel; add_edge N1 -[nmod]-> N2 }
  }

% Proper nouns governing another proper noun with a punctuation mark between them.
  rule propn_mod_propn-punct {
    pattern {
      N1 [upos=PROPN];
      N2 [upos=PROPN]; mod_rel: N1 -[mod]-> N2;
      PUNCT[upos=PUNCT]; N1 << PUNCT; PUNCT << N2
    }
    without { N2 -[case]-> * }
    commands { del_edge mod_rel; add_edge N1 -[nmod]-> N2 }
  }

% Proper nouns expressing person names.
  rule propn_mod_propn {
    pattern {
      N1 [upos=PROPN|X];
      N2 [upos=PROPN|X]; mod_rel: N1 -[mod]-> N2
    }
    without { PUNCT[upos=PUNCT]; N1 << PUNCT; PUNCT << N2 }
    commands { del_edge mod_rel; add_edge N1 -[flat:name]-> N2 }
  }
}

% =============================================================================================
% Changing dependency labels from the SSQ format to the UD format for loose joining relations.
package label_loosejoin_sq_ud {

% Vocative dependents of verbs.
  rule vocative {
    pattern { N[upos=NOUN|PROPN|PRON];modvoc_rel: V -[mod.voc]-> N }
    commands { del_edge modvoc_rel; add_edge V -[vocative]-> N }
  }

% Dislocated elements.
  rule dislocated {
    pattern { dis_rel: V -[dis]-> DIS }
    commands { del_edge dis_rel; add_edge V -[dislocated]-> DIS }
  }

% Parataxis dependency to a finite clause.
  rule parataxis1 {
    pattern { V[upos=VERB,VerbForm=Fin]; mod_rel: C -[mod]-> V }
    commands { del_edge mod_rel; add_edge C -[parataxis]-> V }
  }

% Parataxis dependency to a nominal term.
  rule parataxis2 {
    pattern {
      N[upos=NOUN|PRON|PROPN]; PRED[upos=ADJ|VERB];
      mod_rel: PRED -[mod]-> N
    }
    without { N -[case|mark]-> * }
    commands { del_edge mod_rel; add_edge PRED -[parataxis]-> N }
  }

% Parenthetical clauses.
  rule parataxis_modinc {
    pattern { modinc_rel: C -[mod.inc]-> INC }
    commands { del_edge modinc_rel; add_edge C -[parataxis]-> INC }
  }

 }

% =============================================================================================
% Dependencies that apply to various categories.
package label_transcat_sq_ud {

% Adverbs that modify other words.
  rule cat_mod_adv (lex from "lexicons/negation-adverb.lp") {
    pattern {ADV[upos=ADV|INTJ];mod_rel: GOV -[mod]-> ADV }
    without { ADV[lemma="ne", mwelemma <> "ne_serait-ce_que"] }
    without { ADV[lemma="ne", !mwelemma] }
    without { NE[upos=ADV, lemma = "ne"];GOV -[advmod|mod]-> NE;ADV[lemma=lex.neg] }
    commands { del_edge mod_rel; add_edge GOV -[advmod]-> ADV }
  }

  rule cat_mod_intj {
    pattern { ADV[upos=INTJ]; mod_rel: GOV -[mod]-> ADV }
    commands { del_edge mod_rel; add_edge GOV -[discourse]-> ADV }
  }

% Punctuation dependencies.
  rule cat_ponct {
    pattern { GOV[]; PONCT[upos=PUNCT];punct_rel: GOV -[ponct]-> PONCT }
    commands { del_edge punct_rel; add_edge GOV -[punct]-> PONCT }
  }

% Punctuation dependencies.
  rule cat_ponct_dep {
    pattern { GOV[]; DEP[upos<>PUNCT]; e: GOV -[ponct]-> DEP }
    commands { del_edge e; add_edge GOV -[dep]-> DEP }
  }

% Multi-word expressions
  rule depcpd {
    pattern { e: G -[dep_cpd]-> D }
    commands { del_edge e; add_edge G -[fixed]-> D }
  }


% Negation adverbs playing the role of modifiers for verbs.
  rule verb_mod_adv-neg (lex from "lexicons/negation-adverb.lp") {
    pattern {
      V[upos=VERB]; NE[upos=ADV, lemma ="ne"];
      V -[advmod]-> NE;
      NEG[upos=ADV,lemma=lex.neg];
      mod_rel: V -[mod]-> NEG
    }
    commands {
      del_edge mod_rel;
      add_edge V -[advmod]-> NEG; NEG.Polarity=Neg;
    }
  }

% Negation "ne".
  rule verb_mod_ne {
    pattern {
      V[upos=VERB]; NE[upos=ADV, lemma ="ne"];
      mod_rel: V -[mod]-> NE
    }
    commands {
      del_edge mod_rel;
      add_edge V -[advmod]-> NE; NE.Polarity=Neg
    }
  }

% Prepostional locutions behaving as adverbs.
  rule advmod-prep {
    pattern {
      N[upos=NOUN]; mod_rel: N -[mod]-> PREP;
      PREP[upos=ADP]; PREP -[fixed]-> *
    }
    commands { del_edge mod_rel; add_edge N -[advmod]-> PREP }
  }
}

% =============================================================================================
% Change the head of predicative complements.
package cop {

% Precondition : a verb has an ATS complement.
% Action : the complement becomes the governor of the verb.
  rule verb_ats {
    pattern {
      V[upos=VERB, lemma="être"];
      xcomp_rel: V -[ats|xcomp]-> ATS
    }
    without { ATS[upos=VERB, VerbForm=Inf|Fin] }
    commands {
      del_edge xcomp_rel;
      shift V =[1<>parseme|frsemcor]=> ATS;
      add_edge ATS -[cop]-> V;
      V.upos=AUX
    }
  }

}
