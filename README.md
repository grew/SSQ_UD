# Seq2UD

**Seq\_to\_UD** is a Graph Rewriting System (GRS) for conversion between two annotation formats used for French data:

 * the **Sequoia** format is used in projects [Deep-sequoia](http://deep-sequoia.inria.fr) and FTB-SPMRL
 * the **UD** format is used in [Universal Dependencies project](http://universaldependencies.org/)

## Usage
To use the GRS, the **grew** software must be installed (see [Grew webpage](http://grew.fr)).

### Conversion from Sequoia to UD

```shell
grew transform -grs grs/ssq_to_ud/main.grs -i <input_file> -o <output_file>
```

If the GRS fails to convert some relation, it changes it with a `FAIL_` prefix

The full procedure to produce UD_French-Sequoia version 2.3 is described in `HOWTO_produce_UD_French-Sequoia-2.3.md`

The conversion system is used to produce [UD_French-Sequoia](https://github.com/UniversalDependencies/UD_French-Sequoia) and [UD_French-FQB](https://github.com/UniversalDependencies/UD_French-FQB).

### Conversion from UD to Sequoia

:warning: This system is outdated and is was not updated since UD version 1. Please contact us via an [issue](https://gitlab.inria.fr/grew/SSQ_UD/issues) if you need to use the system.


