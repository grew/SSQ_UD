GREW=grew_dev

self_doc:
	@echo "================================================================================================"
	@echo "Requirement:"
	@echo " - add a (link to) file sequoia.surf.conll with sequoia surface annotation"
	@echo " - add a link to the folder UD_French-Sequoia/"
	@echo " - add a link to the folder UD_tools/"
	@echo ""
	@echo "Targets:"
	@echo " - convert : produce the conversion of seq to the UD annoation (results in _build/ud-***.conllu)"
	@echo " - validate : UD validation of the produced files _build/ud-***.conllu"
	@echo " - diff : Unix diff of the produced files against files in UD_French-Sequoia"
	@echo " - opendiff : Graphical diff of the produced files against files in UD_French-Sequoia"
	@echo " - validate : UD validation of the produced files _build/ud-***.conllu"
	@echo " - install : copy files _build/ud-***.conllu in the folder UD_French-Sequoia"
	@echo "================================================================================================"

convert:
	mkdir -p _build
	@echo "Apply GRS to current Sequoia…"
	$(GREW) transform -grs grs/ssq_to_ud/main.grs -i sequoia.surf.conll -o _build/ud.conllu
	cat _build/ud.conllu | udapy -s ud.SetSpaceAfterFromText > _build/fr_sequoia-ud.conllu
	@echo "Get Ids from current UD data…"
	grep sent_id UD_French-Sequoia/fr_sequoia-ud-dev.conllu | sed 's/.*=[[:space:]]//g' > _build/dev.ids
	grep sent_id UD_French-Sequoia/fr_sequoia-ud-test.conllu | sed 's/.*=[[:space:]]//g' > _build/test.ids
	grep sent_id UD_French-Sequoia/fr_sequoia-ud-train.conllu | sed 's/.*=[[:space:]]//g' > _build/train.ids
	@echo "Splitting of the current Sequoia…"
	conll_tool split _build/fr_sequoia-ud.conllu _build/dev.ids _build/fr_sequoia-ud-dev.conllu
	conll_tool split _build/fr_sequoia-ud.conllu _build/test.ids _build/fr_sequoia-ud-test.conllu
	conll_tool split _build/fr_sequoia-ud.conllu _build/train.ids _build/fr_sequoia-ud-train.conllu

convert_parseme:
	mkdir -p _build
	@echo "Apply GRS to current Sequoia…"
	$(GREW) transform -cupt -grs grs/ssq_to_ud/main.grs -i sequoia.surf.parseme.cupt -o _build/fr_sequoia-ud.parseme.cupt
	@echo "*** TODO *** add SpaceAfter=No (combine udapy / cupt)"

install_ds:
	cp _build/fr_sequoia-ud.conllu /users/guillaum/gitlab/deep-sequoia/ud
	cp _build/fr_sequoia-ud.parseme.cupt /users/guillaum/gitlab/deep-sequoia/ud


validate:
	UD_tools/validate.py --lang fr _build/fr_sequoia-ud-test.conllu
	UD_tools/validate.py --lang fr _build/fr_sequoia-ud-dev.conllu
	UD_tools/validate.py --lang fr _build/fr_sequoia-ud-train.conllu

opendiff:
	opendiff UD_French-Sequoia/fr_sequoia-ud-dev.conllu _build/fr_sequoia-ud-dev.conllu
	opendiff UD_French-Sequoia/fr_sequoia-ud-test.conllu _build/fr_sequoia-ud-test.conllu
	opendiff UD_French-Sequoia/fr_sequoia-ud-train.conllu _build/fr_sequoia-ud-train.conllu

diff:
	diff UD_French-Sequoia/fr_sequoia-ud-dev.conllu _build/fr_sequoia-ud-dev.conllu || true
	diff UD_French-Sequoia/fr_sequoia-ud-test.conllu _build/fr_sequoia-ud-test.conllu || true
	diff UD_French-Sequoia/fr_sequoia-ud-train.conllu _build/fr_sequoia-ud-train.conllu || true

install:
	cp -f _build/fr_sequoia-ud-dev.conllu UD_French-Sequoia/fr_sequoia-ud-dev.conllu
	cp -f _build/fr_sequoia-ud-test.conllu UD_French-Sequoia/fr_sequoia-ud-test.conllu
	cp -f _build/fr_sequoia-ud-train.conllu UD_French-Sequoia/fr_sequoia-ud-train.conllu

# -------------------------------------------------------------------
test:
	head -2365 sequoia.surf.conll > short_SI.conll
	$(GREW) transform -grs grs/ssq_to_ud/main.grs -i short_SI.conll -o short_U.conll
	$(GREW) transform -grs grs/ud_to_ssq/main.grs -i short_U.conll -o short_SF.conll

gui1:
	$(GREW) gui -grs grs/ssq_to_ud/main.grs -i short_SI.conll

gui2:
	$(GREW) gui -grs grs/ud_to_ssq/main.grs -i short_U.conll

test-diff:
	opendiff short_SI.conll short_SF.conll

# -------------------------------------------------------------------
ud:
	$(GREW) transform -grs grs/ud_to_ssq/main.grs -strat fail -i UD_French-GSD/fr_gsd-ud-dev.conllu -o ud-dev.conll
	$(GREW) transform -grs grs/ud_to_ssq/main.grs -strat fail -i UD_French-GSD/fr_gsd-ud-test.conllu -o ud-test.conll
	$(GREW) transform -grs grs/ud_to_ssq/main.grs -strat fail -i UD_French-GSD/fr_gsd-ud-train.conllu -o ud-train.conll

gui_ud:
	$(GREW) gui -grs grs/ud_to_ssq/main.grs -i UD_French-GSD/fr_gsd-ud-train.conllu
gui_udd:
	$(GREW) gui -grs grs/ud_to_ssq/main.grs -i UD_French-GSD/fr_gsd-ud-dev.conllu
gui_udt:
	$(GREW) gui -grs grs/ud_to_ssq/main.grs -i UD_French-GSD/fr_gsd-ud-test.conllu
