## Télécharger le corpus au format Sequoia
La version courante du corpus:

* `wget https://gitlab.inria.fr/sequoia/deep-sequoia/raw/master/trunk/sequoia.surf.conll`

## Appliquer la transformation Seq2ud
* `grew_dev -det -grs ../grs/main.grs -seq ssq_to_ud -i sequoia.surf.conll -f sequoia.ud.conll `

## Faire le diff
* `opendiff 2017_05_24_sequoia.ud.conll sequoia.ud.conll`

## Commande pour recréer l'ancienne version:
revenir dans git à : 20d799cc74ae91faed3c0c1e0f22ee80b745dd76, puis:

* `grew_dev -det -grs ../rewriting_rules/main.grs -seq sequoia_to_ud -i sequoia.surf.conll -f bruno.conll`

## Clean

* `rm -f sequoia.ud.conll sequoia.surf.conll`