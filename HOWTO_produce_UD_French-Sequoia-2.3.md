# HOWTO produce UD_French-Sequoia (version 2.3)

## Clone the Conversion GRS (at tag `UD-2.3`)
 * `git clone https://gitlab.inria.fr/grew/SSQ_UD.git --branch UD-2.3 --single-branch`
 * `cd SSQ_UD` 

## Download original Sequoia corpus (at tag `UD-2.3`)
 * `wget https://gitlab.inria.fr/sequoia/deep-sequoia/raw/UD-2.3/trunk/sequoia.surf.conll`

## Apply the transformation
 * `grew transform -grs grs/ssq_to_ud/main.grs -i sequoia.surf.conll -o tmp.conllu`
 * `cat tmp.conllu | udapy -s ud.SetSpaceAfterFromText > fr_sequoia-ud-all.conllu` with the [udapi](https://github.com/udapi/udapi-python) tool
 * `rm -f tmp.conllu`

Finally the file `fr_sequoia-ud-all.conllu` contains the same data (but not split in 3 subfiles) than in the official version of [UD_French-Sequoia](https://github.com/UniversalDependencies/UD_French-Sequoia).
